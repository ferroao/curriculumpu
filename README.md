<!-- README.md is generated from README.Rmd. Please edit that file -->

<img src=assets/logo.png align="right" width="12%">

# CV\_yml <br></br>Rmarkdown for creating a CV <br></br><br></br><br></br>

## Note

This repository will be dedicated to host my personal data, while the
generic version is hosted in <https://github.com/fernandoroa/CV_yml>

## Structure

To access this curriculum: <https://ferroao.gitlab.io/curriculumpu>

The goal of CV\_yml is to make an .html curriculum and .pdf using `yml`
files.

To define languages and profiles, use the file
`custom/yml/shared_params.yml`

Use optionally the folder `custom/figures` if you need any, and `.bib`
files in the folder `custom/bib`.

## folders: `data` and `config`

Fields in `custom/yml/data` and `custom/yml/config` yml files should
match. Use files in `custom/yml/data` folder to add your information,
and files in `custom/yml/config` folder to configure how to show it.

## folder `dictionaries`

Chapters and field names for several languages are configured in folder
`custom/yml/dictionaries`
